from django.contrib import admin
from books.models import Book, Magazine, Genre, Issue

# Register your models here.
admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Genre)
admin.site.register(Issue)
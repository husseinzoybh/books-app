from django import forms
from books.models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "title",
            "author",
            "number_page",
            "isbn",
            "cover",
            "in_print",
            "publish_date",
        ]

class Magazineform(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []
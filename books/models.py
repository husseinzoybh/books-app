from pyexpat import model
from django.db import models

# Create your models here.

class Genre(models.Model):
    genre_name = models.CharField(max_length=200)

    def __str__(self):
        return self.genre_name

class Book(models.Model):
    title = models.CharField(max_length=200, null=True)
    author = models.CharField(max_length=200, null=True)
    number_page = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    publish_date = models.SmallIntegerField(null=True)

    def __str__(self):
        return self.title + " by " + self.author



class Magazine(models.Model):
    title = models.CharField(max_length=200, null=True)
    release_cycle = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=200, null=True)    
    cover = models.URLField(null=True, blank=True)
    genre = models.ManyToManyField('Genre', related_name='magazines')
   
    

    def __str__(self):
        return self.title


class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name='magazine', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    date_publish = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title
    

   

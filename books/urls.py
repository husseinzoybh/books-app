from django.contrib import admin
from django.urls import path, include
from books.views import show_books, create_book, show_a_book
from .views import edit_view, delete_book, show_a_magazine, edit_magazine, delete_magazine
from books.views import show_magazines, create_magazines, show_a_magazine, show_genres_details

from django.contrib.auth import views as auth_views




urlpatterns = [
    path("", show_books, name = 'book_list'),
    path('create/', create_book, name = 'create'),
    path('<int:pk>/' , show_a_book, name = "show_book_details"),
    path('<int:pk>/edit/', edit_view, name = 'book_edit'),
    path('<int:pk>/delete/', delete_book, name = 'book_delete'),
    path("magazines/", show_magazines, name = 'magazines_list'),
    path("magazines/create", create_magazines, name = 'magazine_new'),
    path('magazines/<int:pk>/', show_a_magazine, name = "magazine_details"),
    path('magazines/<int:pk>/edit/', edit_magazine, name = 'magazine_edit'),
    path('magazines/<int:pk>/delete/', delete_magazine, name = 'magazine_delete'),
    path('magazines/<int:pk>/genres/', show_genres_details, name = "genres_details"),
    path('accounts/login/', auth_views.LoginView.as_view(), name= "login")
   
    
        
] 
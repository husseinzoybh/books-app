from __future__ import generator_stop
from contextlib import redirect_stderr
from django.shortcuts import render
from books.models import Book, Genre, Magazine
from books.forms import BookForm, Magazineform
from django.shortcuts import redirect
from django.shortcuts import (get_object_or_404,render,HttpResponseRedirect)


# Create your views here.
def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def create_book(request):
    context = {}
    form = BookForm (request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('book_list')
        
    context ['form'] = form
    return render(request, 'books/create.html', context)

def show_a_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book_var" : book
    }
    if request.method =="POST":
        
        book.delete()
        
        return redirect("book_list")
    return render(request, "books/details.html", context)

def edit_view(request, pk):
    context ={}
    obj = get_object_or_404(Book, id=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect("book_list")
    context["form"] = form
 
    return render(request, "books/edit.html", context)


def delete_book(request, pk):
    context ={}    
    obj = get_object_or_404(Book, id = id)
    if request.method =="POST":
        
        obj.delete()
        
        return HttpResponseRedirect("book_list")
 
    return render(request, "books/details.html", context)

def show_magazines(request):
    mags = Magazine.objects.all()
    context = {
        "magazines_var": mags
    }
    return render(request, "magazines/magazines_list.html", context) 

def create_magazines(request):
    context = {}
    form = Magazineform (request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('magazines_list')
        
    context ['form'] = form
    return render(request, 'magazines/magazines_create.html', context)

def show_a_magazine(request, pk):
    mag = Magazine.objects.get(pk=pk)
    context = {
        "magazine_var" : mag
    }
    if request.method =="POST":
        
        mag.delete()
        
        return redirect("magazines_list")
    return render(request, "magazines/magazines_details.html", context)

def edit_magazine(request, pk):
    context ={}
    obj = get_object_or_404(Magazine, id=pk)
    form = Magazineform(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect("magazines_list")
    context["form"] = form
    return render(request, "magazines/magazines_edit.html", context)

def delete_magazine(request, pk):
    context ={}    
    obj = get_object_or_404(Magazine, id = pk)
    if request.method =="POST":
        
        obj.delete()
        
        return HttpResponseRedirect("magazines_list")
 
    return render(request, "magazines/magazines_delete.html", context)

def show_genres_details(request, pk):    
    genres = Genre.objects.get(pk=pk)
    context = {
        "genre_var": genres
    }
    return render(request, "magazines/magazines_genres.html", context) 

